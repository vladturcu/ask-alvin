import { useState } from 'react';

const { Configuration, OpenAIApi } = require("openai");

const configuration = new Configuration({
	apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(configuration);



export default function Home() {
	const [questions, setQuestions] = useState([])
	const [input, setInput] = useState("");

	const handleChange = async (e) => {
		e.preventDefault();
		setInput(e.target.value);
	}

	const handleSubmit = async (e) => {
		e.preventDefault();
		const question = input.includes('?') ? input : input + "?";
		const response = await openai.createCompletion("text-davinci-001", {
			prompt: `Marv is a chatbot that reluctantly answers questions corectly with sarcastic and rude responses:\n\nYou:${question}`,
			temperature: 1,
			max_tokens: 60,
			top_p: 1,
			frequency_penalty: 0.5,
			presence_penalty: 0.0,
		});
		const answer = response.data.choices[0].text.split("Marv:")[1];

		setQuestions([ { question, answer: answer }, ...questions ]);
		setInput("")
	}
	console.log('the API key is: ', process.env.OPENAI_API_KEY);
	return (
		<>
			<header className="header container-fluid d-flex justify-content-center align-items-center">
				<h1 className="logo">Ask Alvin</h1>
			</header>
			<div className="container">
				<div className="row">
					<div className="col-md-12 d-flex justify-content-center">
						<div className="chatbox">
							<form onSubmit={handleSubmit}>

								<div className="form">
									<input type="text" onChange={handleChange} value={input} className="form__input" placeholder="Type your question..." />
									<input type="submit" value="Ask" className="form__submit" />
								</div>

								<div className="mt-5">
									{questions.map((question, index) => (
										<div key={index} className="chatbox__message mt-3">
											<div className="chatbox__message__header">
												{question.question}
											</div>
											<div className="chatbox__message__answer">
												{question.answer}
											</div>
										</div>
									))}
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}


